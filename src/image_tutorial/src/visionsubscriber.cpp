#include <ros/ros.h>
#include "image_transport/image_transport.h"
#include "opencv2/highgui/highgui.hpp"
#include "cv_bridge/cv_bridge.h"
#include "opencv2/opencv.hpp"
using namespace cv;
using namespace std;
// // callback function to just display the image that was being published
// void imageCallback(const sensor_msgs::ImageConstPtr& msg)
// {
// 	ROS_INFO("Image was received in the subscribe node ! ");
// 	try{
// 		// cv::imshow("view",cv_bridge::toCvShare(msg,"bgr8")->image);
// 		static cv_bridge::CvImage cv_image.image = cv_bridge::toCvShare(msg,"bgr8")->image;
// 		cv_image.encoding = "bgr8";
// 		static sensor_msgs::Image ros_image;
//     	cv_image.toImageMsg(ros_image);

// 		ros::Publisher pub = nh.advertise<sensor_msgs::Image>("/static_image", 1);
// 		pub.publish(ros_image);
// 		// cv::waitKey(30);
// 	}
// 	catch(cv_bridge::Exception& e){
// 		ROS_ERROR("Could not covert from '%s' to 'bgr8'.",msg->encoding.c_str());
// 	}
// }

// int main(int argc,char **argv){
// 	ros::init(argc,argv, "image_listener");
// 	ros::NodeHandle nh;
// 	cv::namedWindow("view");
// 	cv::startWindowThread();
// 	image_transport::ImageTransport it(nh);
// 	image_transport::Subscriber sub = it.subscribe("/kinect2/hd/image_color",1,imageCallback);
// 	ros::spin();
// 	cv::destroyWindow("view");
// }
 
// comparison function object
bool compareContourAreas (const vector<Point> contour1,const vector<Point> contour2 ) {
    double i = fabs( contourArea(cv::Mat(contour1)) );
    double j = fabs( contourArea(cv::Mat(contour2)) );
    return ( i > j );
}

class SubscribeandPublish{
private:
	ros::NodeHandle nh;
	image_transport::ImageTransport it;
	image_transport::Subscriber sub;
	image_transport::Publisher pub_r,pub_g,pub_b,pub;
	cv_bridge::CvImagePtr cv_ptr;
	sensor_msgs::ImagePtr msg_blue,msg_green,msg_red,msg_rgb;


public:
	// constructor
	SubscribeandPublish():it(nh){
		// cv::namedWindow("test");
		sub = it.subscribe("/kinect2/hd/image_color",1,&SubscribeandPublish::imageCallback,this);
		pub_r = it.advertise("/cup_red",1);
		pub_g = it.advertise("/cup_green",1);
		pub_b = it.advertise("/cup_blue",1);
		pub = it.advertise("/cup_rgb",1);
	}

	void imageCallback(const sensor_msgs::ImageConstPtr& msg){
		try{
			cv_ptr = cv_bridge::toCvCopy(msg,sensor_msgs::image_encodings::BGR8);
			// function to convert image to hsv and segment colors.
			segment_color(cv_ptr);
			// cv::startWindowThread();
			// cv::imshow("test",cv_ptr->image);
			// cv::waitKey(30);
		}
		catch(cv_bridge::Exception& e){
			ROS_ERROR("cv_bridge exception: %s",e.what());
		}

		// publish output modified image stream, all 3 colors separate
		pub_r.publish(msg_red);
		pub_g.publish(msg_green);
		pub_b.publish(msg_blue);
		pub.publish(msg_rgb);	

	}

	void segment_color(const cv_bridge::CvImagePtr& (cv_ptr)){
	if(!cv_ptr) return;
	cv::Mat hsv_img,hsv_img_mask;
	cv::Mat blue,green,red;
	// HSV range blue
	cv::Scalar minHSV_b = cv::Scalar(90,100,50);
	cv::Scalar maxHSV_b = cv::Scalar(130,255,255);
	// HSV range green
	cv::Scalar minHSV_g = cv::Scalar(40,100,100);
	cv::Scalar maxHSV_g = cv::Scalar(80,255,255);
	// HSV range red
	cv::Scalar minHSV_r = cv::Scalar(130,140,50);
	cv::Scalar maxHSV_r = cv::Scalar(180,255,255);

	
	cv::cvtColor(cv_ptr->image,hsv_img,cv::COLOR_BGR2HSV); 
	// extract blue,green,red
	cv::inRange(hsv_img,minHSV_b,maxHSV_b,hsv_img_mask);
	cv::bitwise_and(hsv_img, hsv_img, blue,hsv_img_mask);

	cv::inRange(hsv_img,minHSV_g,maxHSV_g,hsv_img_mask);
	cv::bitwise_and(hsv_img, hsv_img, green,hsv_img_mask);
	
	cv::inRange(hsv_img,minHSV_r,maxHSV_r,hsv_img_mask);
	cv::bitwise_and(hsv_img, hsv_img, red,hsv_img_mask);

	// convert back to BGR
	cv::cvtColor(blue,blue,cv::COLOR_HSV2BGR); 
	cv::cvtColor(green,green,cv::COLOR_HSV2BGR);
	cv::cvtColor(red,red,cv::COLOR_HSV2BGR);

	// blob detect and extract colored blob
	blob_detection_extract(green);
	blob_detection_extract(red);
	blob_detection_extract(blue);

	msg_blue = cv_bridge::CvImage(std_msgs::Header(), "bgr8", blue).toImageMsg();
	msg_green = cv_bridge::CvImage(std_msgs::Header(), "bgr8", green).toImageMsg();
	msg_red = cv_bridge::CvImage(std_msgs::Header(), "bgr8", red).toImageMsg();
	msg_rgb = cv_bridge::CvImage(std_msgs::Header(), "bgr8", red+blue+green).toImageMsg();
	// cv::imshow("test",blue);
	// cv::waitKey(30);	
	}

	void blob_detection_extract(cv::Mat& color){
		static vector< vector<Point> > contours_color;
		static cv::Mat color_dummy;
		static vector< vector<Point> > biggestContour_color;
		// convert rgb to greyscale
		cvtColor(color, color_dummy, CV_BGR2GRAY);
		// threshold the image
		threshold(color_dummy,color_dummy,10,255,CV_THRESH_BINARY);
		// find the contours
		findContours(color_dummy,contours_color, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
		sort(contours_color.begin(),contours_color.end(),compareContourAreas);

		biggestContour_color.push_back(contours_color[0]);

		Mat mask_color = Mat::zeros(color_dummy.rows, color_dummy.cols, CV_8UC1);
		// bitwise_not(mask_color,mask_color);
		drawContours(mask_color, biggestContour_color, -1, Scalar(255), CV_FILLED);
		
		static Mat final_color;
		final_color = Mat::zeros(color_dummy.rows, color_dummy.cols,CV_8U);
		cv::bitwise_and(color,color,final_color,mask_color);
		color = final_color;
		// cv::imshow("mask",final_color);
		// cv::waitKey(30);
		contours_color.erase(contours_color.begin(),contours_color.end());
		biggestContour_color.erase(biggestContour_color.begin(),biggestContour_color.end());
	}

	// desctuctor for the class
	~SubscribeandPublish(){
		// cv::destroyWindow("test");
	}
};

int main(int argc, char **argv){
	ros::init(argc,argv, "image_listener");
	SubscribeandPublish subnpub;
	ros::spin();

	return 0;
}
#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/home/haresh/.mujoco/mjpro150/bin:/usr/lib/nvidia-384:/home/haresh/.mujoco/mjpro150/bin:/usr/local/cuda/extras/CUPTI/lib64/"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig"
export PWD="/home/haresh/BWI_tutorial/build"
export ROS_PACKAGE_PATH="/home/haresh/BWI_tutorial/src:/opt/ros/kinetic/share"